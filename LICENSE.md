# Licenca

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licenca Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Projektna naloga pri predmetu OVS</span> avtorjev 
<a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/aleksandra-franc" property="cc:attributionName" rel="cc:attributionURL">Aleksandra Franc</a>,
<a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/kristina-veljkovic" property="cc:attributionName" rel="cc:attributionURL">Kristina Veljkovič</a> in
<a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/martin-vuk" property="cc:attributionName" rel="cc:attributionURL">Martin Vuk</a>
 je objavljeno pod <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">licenco Creative Commons Priznanje avtorstva-Nekomercialno-Deljenje pod enakimi pogoji 4.0 Mednarodna</a>.<br />Ustvarjeno na podlagi dela, dostopnega na <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/ul-fri/ovs/projekt/" rel="dct:source">https://gitlab.com/ul-fri/ovs/projekt/</a>.<br />Dodatna dovoljenja, ki presegajo obsege te licence, so dostopna na <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md" rel="cc:morePermissions">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md</a>.


## Primer statistične obdelave podatkov: t-test za neodvisna vzorca

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licenca Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>

<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Primer statistične obdelave podatkov: t-test za neodvisna vzorca</span> avtorja <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/kristina-veljkovic" property="cc:attributionName" rel="cc:attributionURL">Kristina Veljkovič</a> je objavljeno pod <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">licenco Creative Commons Priznanje avtorstva-Nekomercialno-Brez predelav 4.0 Mednarodna</a>.<br />Ustvarjeno na podlagi dela, dostopnega na <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/t-test.pdf" rel="dct:source">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/t-test.pdf</a>.

Dodatna dovoljenja, ki presegajo obsege te licence, so dostopna na <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md" rel="cc:morePermissions">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md</a>.


## Primer statistične obdelave podatkov: regresija

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licenca Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>

<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Primer statistične obdelave podatkov: regresija</span> avtorja <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/kristina-veljkovic" property="cc:attributionName" rel="cc:attributionURL">Kristina Veljkovič</a> je objavljeno pod <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">licenco Creative Commons Priznanje avtorstva-Nekomercialno-Brez predelav 4.0 Mednarodna</a>.<br />Ustvarjeno na podlagi dela, dostopnega na <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/regresija.pdf" rel="dct:source">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/regresija.pdf</a>.

Dodatna dovoljenja, ki presegajo obsege te licence, so dostopna na <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md" rel="cc:morePermissions">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md</a>.

## Uvod v statistični softwer R

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licenca Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Uvod v statistični softwer R</span> avtorja <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/kristina-veljkovic" property="cc:attributionName" rel="cc:attributionURL">Kristina Veljkovič</a> je objavljeno pod <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">licenco Creative Commons Priznanje avtorstva-Nekomercialno-Brez predelav 4.0 Mednarodna</a>.<br />Ustvarjeno na podlagi dela, dostopnega na <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/IntroR.pdf" rel="dct:source">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/IntroR.pdf</a>.

Dodatna dovoljenja, ki presegajo obsege te licence, so dostopna na <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md" rel="cc:morePermissions">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md</a>.

## Podatki

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licenca Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Podatkovna zbirka</span> avtorja <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/kristina-veljkovic" property="cc:attributionName" rel="cc:attributionURL">Kristina Veljkovič</a> je objavljena pod <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">licenco Creative Commons Priznanje avtorstva-Nekomercialno-Brez predelav 4.0 Mednarodna</a>.<br />Ustvarjeno na podlagi dela, dostopnega na <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/podatki/" rel="dct:source">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/podatki/</a>.

Dodatna dovoljenja, ki presegajo obsege te licence, so dostopna na <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md" rel="cc:morePermissions">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md</a>.

## Pridobitev dodatnih dovoljenj
Za dodatna dovoljenja, ki presegajo obsege te licence, kontaktirajte avtorje.