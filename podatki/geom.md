# Rezultati testa geometrije

Podatki: [geom.csv](geom.csv)

## Opis

Meritve števila točk na državnem izpitu iz geometrije na naslednjih vzorcih dijakov ene šole

1. vzorec 35 dijakov, ki so se učili geometrijo pri prvem inštruktorju in
2. vzorec 35 dijakov, ki so se učili geometrijo pri drugem inštruktorju.

## Format

Baza podatkov s 70 meritvami dveh spremenljivk

* *instr* je nominalna spremenljivka, z numeričnima kodama: 1=prvi, 2=drugi inštruktor.
* *test* je numerična diskretna spremenljivka, ki predstavlja število točk na testu geometrije.

## Raziskovalna domneva

Inštruktorja šole ne pripravita dijake enako dobro za državni izpit iz geometrije.