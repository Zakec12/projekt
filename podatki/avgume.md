# Življenjska doba avtomobilskih gum

Podatki: [avgume.csv](avgume.csv)

## Opis

Meritve življenjske dobe na vzorcu 25 avtomobilskih gum, proizvedenih po novi metodi in na
vzorcu 25 gum, proizvedenih po standardni metodi.

## Format

Baza podatkov s 50 meritvami dveh spremenljivk

* *metoda* je nominalna spremenljivka, z numeričnima kodama: 1=nova metoda, 2=stan-
dardna metoda.
* *zdoba* je numerična zvezna spremenljivka, ki predstavlja življenjsko dobo avtomobilskih
gum (v 1000 km)

## Raziskovalna domneva

Nova metoda proizvodnje avtomobilskih gum vpliva na povečanje njihove življenjske dobe.